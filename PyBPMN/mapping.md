# PyBPMN <-> BPSim
Sezione che racchiude le analogie tra PyBPMN e BPSim bottom-up

| PyBPMN | BPSim |
|--------|-------|
|NFP_Boolean|BooleanParameter|
|NFP_Integer (-timeUnit)|NumericParameter|
|NFP_String|StringParameter|
|NFP_DateTime|DateTimeParameter|
|NFP_Real (-timeUnit,+precision)|FloatingParameter|
|NFP_Duration (con timeUnit)|FloatingParameter|
|NFP_Frequency|(Forse con qualche magia nei calendar)|
|NFP_DaFrequency|none (direi che è anche inutile, cosiderando che non viene mai utilizzata nel modello PyBPMN)|
|(Metodi di) NFP_CommonType|DistributionParameter|
|none<sup>1</sup>|ExpressionParameter|
|none|EnumParameter|

<sup>1</sup>Non direttamente mappata.

Grazie al parametro `source` di **`NFP_CommonType`** è possibile considerare
questi oggetti come una sorta di misto tra **`ParameterValue`** e 
**`ResultRequest`** in BPSim. In particolare si possono ottenere 
DistributionParameter e ConstantParameter.

Quindi, considerando la natura di un `Parameter` in BPSim (composto da 
ResultRequest e ParameterValue) è possibile una certa corrispondenza tra 
`Parameter` in BPSim e diversi oggetti che derivano da NFP_CommonType in PyBPMN.

Le diverse viste di PyBPMN non si trovano in corrispondenza biunivoca con quelle
presentate da BPSim:

## Workload

|PyBPMN|BPSim|
|------|-----|
|PeriodicPattern.period|InterTriggerTimer|
|ClosedPattern.extDelay|TimeParameters.TransferTime/LagTime|

ArrivalPattern -> TransferTime/LagTime

## Performance

|PyBPMN|BPSim|
|------|-----|
|PaService|TimeParameters.Duration/ProcessingTime|
|PaResponse.responseTime|TimeParameters.Duration/ProcessingTime|

## Reliability
BPSim non presenta in alcun punto visioni di affidabilità della piattaforma di
esecuzione

## Resource

|PyBPMN|BPSim|
|------|-----|
|PyRequest|ResourceParameters.Selection<sup>1</sup>|
|PyPerformer|BPMN Resource<sup>2</sup>|
|PyBroker|none|
|PySubsystem|none|

<sup>1</sup> con ExpressionParameter:

1. bpsim:getResource

    > resourceRef è specificato tramite il parametro name
    >
    > serviceQuantity specificato tramite il parametro qty

2. bpsim:orResource
    
    > si tratta di un caso particolare, in quanto, può essere utilizzato per 
    > simulare un PyBroker in cui le possibili risorse sono passate come input
    > della funzione e
    >
    >  stanby -> hot standby
    > 
    > policy -> UntilRunning

3. bpsim:getResourceByRoles

    > non c'è mappatura per PyBPMN

In 1, se il parametro name è un id di risorsa BPMN, possiamo mappare a 
PyPerformer.

<sup>2</sup> PyPerformer.units è settato dal parametro Quantity di BPSim mentre
la risorsa tramite gli oggetti Resource di BPMN.

Ne in BPMN ne in BPSim esistono concetti che assomiglino al PySubsystem, 
pertanto, oggetti di questo tipo possono soltanto essere mappati ad una normale
risorsa.

# Corrispondenze assenti
Tutta la sezione dei parametri legati al costo di BPSim non trova un riscontro
in PyBPMN, così come i PropertyParameter (e di conseguenza la funzione 
getProperty).

Non si trova neanche corrispondenza tra quelli che BPSim chiama 
ScenarioParameters ed elementi di PyBPMN, ne tantomeno tra tutti gli attributi
di (BPSim) Scenario e altri oggetti di PyBPMN.
