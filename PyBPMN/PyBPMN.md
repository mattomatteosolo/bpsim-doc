# PyBPMN

Performability-enabled Business Process Modeling Notation, estende il 
metamodello BPMN ed è istanza di MOF.

Un modello PyBPMN viene realizzato attuando una trasformazione model-to-model 
da un modello BPMN con delle `TextAnnotaion` propriamente formattate.

Il business process è detto in stato *corretto* se è compliant con i requisiti 
e le specifiche. Quando devia da questo, si verifica una **failure** e il 
processo va in *incorrect state*.

Possiamo quindi vedere un processo come una serie di stati osservabili. Un 
**failure** indica che almeno uno degli stati della catena è in *incorrect 
state*. Questo stato è detto **error**. La causa di un errore è detta **fault**
(esistono ovviamente diversi fault, da quello umano, ad una risorsa non 
disponibile, ad un errore di programmazione, ecc).

![Catena di stati](./state_chain.png)

Ovvaimente, un processo è un insieme di elementi BPMN che interagiscono tra 
loro. Nella visione di PyBPMN, un insieme finito di risorse definisce la 
*piattaforma di esecuzione*, che si occupa della vera esecuzione del modello.
Se tale piattaforma fornisce ad un elemento del processo tutte le risorse
necessarie allo stesso, questo elemento è detto essere in *stato corretto*.

Da quanto detto, un processo è in stato corretto se tutti gli elementi che lo
costituiscono sono in stato corretto.

## Basic Data Types

- **PyBaseElement**(/**PyElement**?)
Classe astratta radice nell'albero delle dipendenze delle classi di PyBPMN, 
fornisce:
    - id, identificatore univoco
    - name, nome descrittivo dell'elemento

- **NFP_CommonType**
    Definisce la definizione di distribuzioni di probabilità comuni come metodi
    di classe, in più ai seguenti attributi
    - expr, stringa utilizzata per specificare il valore come un espressione,
        permettendo relazioni funzionali tra valori di differenti attributi,
    - source, enum di tipo SourceKind, specificando come sono stati ottenuti i 
        valori:
        - est -> stimato
        - meas -> misurato
        - calc -> ottenuto tramite un calcolo
        - req -> il valore è richiesto
    - statQ, enum di tipo StatisticalQualifierKinf, specifica la natura 
        statistica del valore:
        - max -> valore massimo
        - min -> valore minimo
        - mean -> valor medio
        - percent -> valore percentile
        - range -> range di possibili valori
        - distrib -> distribuzione di probabilità
    - dir, enum di tipo DirectionKind, utilizzata per specificare una 
        comparazione qualitativa tra valori differenti:
        - incr -> valori "maggiori" indicano una maggiore qualità
        - decr -> il contrario di incr

- **NFP_Real**, **NFP_Boolean**, **NFP_Strin**, **NFP_Integer**,
**NFP_DateTime**, sono estensioni di NFP_CommonType utilizzate per indicare
il tipo dell'attributo `value`. A sua volta, NFP_Real può specializzarsi in:
    - **NFP_Frequency**, specifica un'unità, `unit`, di misura per una frequenza
        espressa in termini di FrequencyUnitKind
    - **NFP_Duration**, indica l'unità, `unit`, di misura di un intervallo 
        temporale secondo quanto specificato in TimeUnitKind
    - **NFP_DaFrequency**, utilizzato per specificare il rateo di occorrenza di
        failure, con unità, `unit`, di misura scelta tra DaFrequencyUnitKind

## Viste di PyBPMN

- **workload**
    Modellare il carico di lavore del processo o dei task associati al
    processo.
- **performance**
    Proprietà legate all'efficienza del processo o del singolo task.
- **reliability** (affidabilità) 
    Proprietà di affidabilità delle risorse associate al processo oppure ai
    singoli task.
- **gestione delle risorse**
    Definisce quali risorse sono utilizzate da quali task, risorse singole, 
    gruppo di risorse, oppure scelta tra un insieme di risorse.

### Workload
Classi per la modellazione del carico di lavoro.

Di questa vista fanno parte le seguenti classi:

- **`GaWorkloadEvent`** Descrive il flusso di richieste rappresentanti il carico
    di lavoro attraverso l'attributo `trace` di tipo GaEventTrace
- **`GaEventTrace`** descrive "trace" che rappresenta il flusso delle richieste:
    - `content` serializzazione della traccia eventi
    - `format` formato della traccia
    - `location` path del file della traccia
- **`ArrivalPattern`** Classe astratta per specificare in quale maniera arrivano
    le richieste
- **`ClosedPattern`** specifica il carico di lavoro in termini di utenti/jobs 
    attivi o potenzialmente attivi
    - `population` numero di utenti/jobs
    - `extDelay` tempo intercorso tra la fine di una risposta e l'inizio della 
        nuova richiesta
- **`OpenPattern`** Specifia il carico di lavoro in termini di arrivi di 
    utenti/jobs
    - `interArrivalTime` tempo tra due arrivi che si succedono
    - `arrivalRate` frequenza media di arrivi
    - `arrivalProcess` nome del processo in arrivo utilizzato dagli strumenti di 
        analisi
- **`PeriodicPattern`** Classe che specifica il carico di lavoro din pattern
    periodico
    - `period` periodo del pattern
    - `jitter` massima deviazionedelle occorrenze
    - `phase` ritardo per la prima occorrenza dell'evento
    - `occurrences` massimo numero di occorrenze per l'evento
- **`AperiodicPattern`**
    - `distribution` distribuzione del pattern
- **`BurstPattern`**, deriva da **`AperiodicPattern`**
    - `minInterarrival` minimo intervallo di tempo tra due occorrenze successive
        di un evento
    - `maxInterarrival` massimo intervallo di tempo tra due occorrenze
        successive di uno stesso evento
    - `minEventIntervall` come `minInterarrival` ma generale per tutti gli 
        eventi
    - `maxEventIntervall` come `maxInterarrival` ma generale per tutti gli
        eventi
    - `burstSize` massimo numero di occorreze per il pattern

### Performance

- **`PaQualification`** Classe astratta per la caratterizzazione per le 
    performance
- **`PaService`** Specifica "le richieste del servizio" (quanto tempo necessita)
    - `serviceTime` tempo richiesto per la realizzazione di una singola 
        richiesta
- **`PaResponse`** Specifica le performances come se fossero viste da un utente
    esterno
    - `throughput` richieste completate per unità di tempo
    - `responseTime` tempo speso per l'esecuzione della richiesta

### Reliability

- **`DaQualification`** Classe astratta per la caratterizzazione di 
    "reliability" affidabilità
- **`DaFault`** Proprietà legate ai fault
    - `rate` rateo delle occorrenze di fault
    - `occurenceProb` probabilità che si verifichi un fault
    - `OccurrenceDist` distribuzione di probabilità dell'occorrenza di un fault 
        in un intervallo di tempo
- **`DaFailure`** Proprietà relative ai failure
    - `rate` rateo di occorrenze di failure
    - `MTTF` tempo medio per una failure
    - `MTTR` tempo medio per riparare da una failure

### Resource
Le altre viste di PyBPMN definiscono proprietà non-funzionali per un modello
BPMN. La vista delle risorse, estendendo le definizioni delle risorse di BPMN,
permette, al momento del design quali risorse reali realizzeranno quali 
attività, oltre a specificare altre proprietà non-funzionali.

Una risorsa PyBPMN può modellare un "worker" reale, delle apparecchiature, una
divisione di una organizzazione, un sistema autonomo, un servizio web, o, in 
generale, una qualsiasi entità in grado di assolvere la richiesta.

- **`PyRequest`** Classe wrapper che specifica 
    - `serviceQuantity` quantità di risorsa richiesta
    - `resourceRef` riferimento alla risorsa che deve eserguire il servizio
- **`PyBaseResource`** classe astratta che rappresenta concettualmente la/e
    risorsa/e da utilizzare e specializza nelle classi che seguono
- **`PyPerformer`** Un vero "worker" che esegue la richiesta
    - `units` numero di "worker" che possono lavorare in parallelo, che 
    costituiscono il "performer"
- **`PyBroker`** Un manager di risorse che le alloca una alla volta. La 
    selezione è dinamica: quando la risorsa correntemente in uso non è più 
    disponibile (per diversi motivi), viene eseguito il processo di selezione
    ed un'altra risorsa può essere utilizzata
    - `alternatives` insieme delle risorse che possono essere selezionate
    - `policy` strategia di selezione per la risorsa
        - **`RoundRobin`** la prossima risorsa viene selezionata per fare un uso
            giusto delle risorse
        - **`UntilRunning`** la risorsa corrente viene utilizzata fino a quando
            non fallisce e quindi si seleziona la prossima nella lista
        - **`BestFirst`** la risorsa corrente viene utilizzata fino a che non se
            ne trova una migliore
    - `standby` specifica la strategia di gestione delle risorse non selezionate
        - *hot standby* strategia col maggiore coefficiente di ridondanza, in 
            cui tutte le risorse sono attive, ma possono anche fallire. Si 
            favoriscono le performance all'affidabilità
        - *cold standby* le risorse in standby sono disattive, quindi il rateo
            di fallimento è da considerare zero, ma è richiesto del tempo di 
            warm up per  ciascuna richiesta quando questa viene selezionata.
            Si tratta quindi del polo opposto all'hot standby, affidabilità >
            performance
        - *warm standby* approccio ibrido, in cui le risorse sono attivie ma 
            scariche, mostrando quindi una minore probabilità di fallimento. 
            Viene considerato come un caso speciale dell'hot standby
- **`PySubsystem`** Un'insieme di rorse che sono tutte richieste per il 
    completamento della richiesta. Questa classe invia sequenzialmente le 
    richieste a tutte le risorse che costituiscono il sistema
    - `components` insieme di risorse che costituiscono il sistema

Si può notare che nella vista delle risorse, PyBPMN presenta un pattern di tipo
composite, in grazie al quale è possibile realizzare slezioni di risorse 
atomiche, o strutture di selezione più complicate, aggiungendo quindi livelli di
complessità. Si può quindi creare una struttura ad albero, le cui foglie sono 
necessariamente dei PyPerformer.

## Descrizione Generale del metamodello
Spiegamo ora come gli elementi precedentemente analizzati possono essere 
utilizzati per specificare proprietà non-funzionali di elementi del modello o di
risorse utilizzate per completare le richieste.

- **`PyData`** Container per tutti gli elementi PyBPMN definiti per il processo.
    Permette di raggruppare tutti gli elementi PyBPMN sotto un siglo nodo nel 
    modello BPMN (in un singolo artifact); eredita da Artifact e risiede nella
    sezione artifacts di BPMN. Permette di raccogliere nello stesso modello sia 
    i dati di simulazione che quelli di esecuzione, permettendo agli analisti di
    apportare le necessarie modifiche in fase di redesign.
    - `configurations` insieme di **`PyConfiguration`**, ognuna delle quali 
        definisce una differente piattaforma di esecuzione
- **`PyConfiguration`** Insieme di proprietà non-funzioni tra loro collegate che
    possono essere richiese fase di design, stimate tramite simulazione, o 
    misurate durante l'esecuzione effettiva. Ogni configurazione può essere 
    presa in considerazione come piattaforma di esecuzione, sia per le 
    simulazioni che per l'esecuzione stessa del processo
    - `refersTo` permette di mettere in relazione diverse configurazioni
    - `elements` insieme di **`PyElement`**, ognuno dei quali specifica un 
        parametro non funzionale del processo di business
- **`PyElement`** Classe astratta che gestisce le proprieta non-funzionali delle
    varie vista sopra descritte, si specializza in PyDescriptor e PyBaseResource
    - `workloadParams` utilizzando la classe **`GaWorkloadEvent`**
    - `performanceParams` utilizzando una classe che deriva da 
        **`PaQualification`**
    - `reliabilityParams` utilizzando una classe che derivi da 
        **`DaQualification`**

    Per specificare la natura dei dati che lo compongono, è necessario l'uso del
    parametro source:
    - `req`, required, il valore deve essere soddisfatto dal sistema (una sorta 
        di requisito o vincolo)
    - `est`, estimated, usato per indicare che il valore di un parametro è stato
        stimato, ad esempio, attraverso una simulazione precedente
    - `calc`, calculated, il valore è ottenuto manipolando un altro parametro
    - `meas`, measured, il valore del parametro è stato misurato a valori 
        collezionati in fase di esecuzione
- **`PyDescriptor`** permette associazioni tra proprietà non funzionali definite
    da elementi di PyBPMN a elementi di tipo `FlowNode` di BPMN.
    - `resources` insieme delle risorse dalla piattaforma di esecuzione
    - `targetRef` id univoco del FlowNode BPMN

### Metodologie di utilizzo del metamodello PyBPMN per caratterizzare BPMN
Esistono due modi per utilizzare PyBPMN:
1. attraverso l'elemento PyDescriptor, si associano tutte le proprietà 
non-funzionali all'elemento BPMN nascondendo quindi le i dettagli della 
piattaforma di esecuzione
2. viene definita la piattaforma di esecuzione eplicitamente attraverso le 
diverse classi messe a disposizione da PyBPMN e soltanto dopo, il PyDescriptor 
associa una o più risorse all'elemento BPMN attraverso un insieme di oggetti
PyRequest

La prima modalità va bene per un'analisi grossolana, la seconda permette diversi
benefici, tra cui:

- maggiori dettagli del comportamento del processo dato che la piattaforma 
sottostante è definita chiaramente
- il PyDescriptor può fare riferimento a diverse risorse del modello per un uso
sequenziale delle stesse
- una risorsa può essere referenziata da più py descriptor, nella stessa maniera
in cui una risorsa fisica può eseguire più task del modello con differenti 
richieste; chiaramente, il fallimento di tale risorsa affligge nella stessa 
maniera tutti gli elementi BPMN che la utilizzano

## Utilizzo dell'estensione
Come anticipato, un modello PyBPMN è ottenuto attraverso trasformazioni 
model-to-model su un modello BPMN con annotazioni (`TextAnnotaion`) 
adeguatamente formattate. Le `TextAnnotaion` che includo valide specifiche
PyBPMN sono dette `PyAnnotation` contenenti una o più definizioni secondo la 
grammatica:

```
<PyDefinition> -> <PyElement> '{' <PyParamList> '}'
<PyElement> -> '<<PyDescriptor>>' 
                | '<<PyPerformer>>' 
                | '<<PyBroker>>' 
                | '<<PySubsystem>>'
<PyParamList> -> <String> '=' <Value> (',' <String> '=' <Value>)*
<Value> -> <Literal> | <ComplexValue> | <CollectionValue>
<Literal> -> <String> | <Number>
<ComplexValue> -> '('<String> '=' <Literal> (',' <String> '=' <Literal>)* ')'
<CollectionValue> -> '(' <Literal> (',' <Literal>)* ')'
```
Con *String* una qualsiasi stringa alfanumerica e *Number* del testo numerico.

Vediamo ora come gestire *\<Value\>* nella grammatica per gli altri elementi non 
specificati di PyBPMN:

- attributi semplici di tipo built-in sono specificati tramite *\<Literal\>*
- attributi che fanno riferimento a tipi PyBPMN sono tradotti in 
*\<ComplexValue\>*
- attributi chefanno riferimento a insiemi di altri elementi sono di tipo 
*\<CollectionValue\>*

quindi

- per tutti i tipi derivati da `PyBaseElement`:
    - `name` e `id` possono essere specificati con una *\<String\>*
    - attributi delle classi derivati da `PaQualification` e `DaQualification`
    da *\<ComplexValue\>*
- `units` di `PyPerformer` possono essere specificati da *\<Number\>*
- `alternatives` di `PyBroker` con *\<CollectionValue\>* con il `name` delle 
corrispondenti risorse
- `components` di `PySubsystem` come `alternatives`
- `resources` di `PyDescriptor`come sopra. In particolare, la quantità di 
"servizio" richiesta può essere specificata tra parentesi quadre alla fine del 
nome della risorsa, i.e. `nome_risorsa[2.0]`

## PyBPMN <-> BPSim
Sezione che racchiude le analogie tra PyBPMN e BPSim bottom-up
| PyBPMN | BPSim |
|--------|-------|
|NFP_Boolean|BooleanParameter|
|NFP_Integer (-timeUnit)|NumericParameter|
|NFP_String|StringParameter|
|NFP_DateTime|DateTimeParameter|
|NFP_Real (-timeUnit,+precision)|FloatingParameter|
|NFP_Duration (con timeUnit)|FloatingParameter|
|NFP_Frequency|(Forse con qualche magia nei calendar)|
|NFP_DaFrequency|none (direi che è anche inutile, cosiderando che non viene mai utilizzata nel modello PyBPMN)|
|(Metodi di) NFP_CommonType|DistributionParameter|
|none<sup>1</sup>|ExpressionParameter|
|none|EnumParameter|

<sup>1</sup>Non direttamente mappata.

Grazie al parametro `source` di **`NFP_CommonType`** è possibile considerare
questi oggetti come una sorta di misto tra **`ParameterValue`** e 
**`ResultRequest`** in BPSim. In particolare si possono ottenere 
DistributionParameter e ConstantParameter.

Quindi, considerando la natura di un `Parameter` in BPSim (composto da 
ResultRequest e ParameterValue) è possibile una certa corrispondenza tra 
`Parameter` in BPSim e diversi oggetti che derivano da NFP_CommonType in PyBPMN.

Le diverse viste di PyBPMN non si trovano in corrispondenza biunivoca con quelle
presentate da BPSim:

### Workload

|PyBPMN|BPSim|
|------|-----|
|PeriodicPattern.period|InterTriggerTimer|
|ClosedPattern.extDelay|TimeParameters.TransferTime/LagTime|

ArrivalPattern -> TransferTime/LagTime
### Performance

|PyBPMN|BPSim|
|------|-----|
|PaService|TimeParameters.Duration/ProcessingTime|
|PaResponse.responseTime|TimeParameters.Duration/ProcessingTime|

### Reliability
BPSim non presenta in alcun punto visioni di affidabilità della piattaforma di
esecuzione

### Resource

|PyBPMN|BPSim|
|------|-----|
|PyRequest|ResourceParameters.Selection<sup>1</sup>|
|PyPerformer|BPMN Resource<sup>2</sup>|
|PyBroker|none|
|PySubsystem|none|

<sup>1</sup> con ExpressionParameter:
1. bpsim:getResource
    > resourceRef è specificato tramite il parametro name
    >
    > serviceQuantity specificato tramite il parametro qty
2. bpsim:orResource
    > si tratta di un caso particolare, in quanto, può essere utilizzato per 
    > simulare un PyBroker in cui le possibili risorse sono passate come input
    > della funzione e
    >
    >  stanby -> hot standby
    > 
    > policy -> UntilRunning
3. bpsim:getResourceByRoles
    > non c'è mappatura per PyBPMN

In 1, se il parametro name è un id di risorsa BPMN, possiamo mappare a 
PyPerformer.

<sup>2</sup> PyPerformer.units è settato dal parametro Quantity di BPSim mentre
la risorsa tramite gli oggetti Resource di BPMN.

Ne in BPMN ne in BPSim esistono concetti che assomiglino al PySubsystem, 
pertanto, oggetti di questo tipo possono soltanto essere mappati ad una normale
risorsa.

## Corrispondenze assenti
Tutta la sezione dei parametri legati al costo di BPSim non trova un riscontro
in PyBPMN, così come i PropertyParameter (e di conseguenza la funzione 
getProperty).
