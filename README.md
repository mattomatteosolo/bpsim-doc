# Documentazione BPSimaaS

Questo repo contiene i file sorgente della documentazion (.tex), l'ultimo
risultato (.pdf) della compilazione, diagrammi disegnati con
[drawio](https://app.diagrams.net/) e note personali.

## Nota

Non ho memorizzato i pacchetti utilizzati per compilare il progetto latex, ego,
è consigliabile visualizzare il pdf.
